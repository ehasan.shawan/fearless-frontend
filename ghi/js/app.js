function createCard(name, description, pictureUrl, startDate, endDate) {
    return `
        <div class="card mt-5">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${startDate} - ${endDate}
            </div>
        </div>
        <style>
            #conferenceCardsContainer {
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
                gap: 20px; /* Space between cards */
                justify-content: center;
                }
              .card-img-top {
                width: 100%;
                max-height: 300px;
                object-fit: cover;
            }
        </style>
    `;
}







window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/'
    const container = document.getElementById("conferenceCardsContainer");


    try {
        const response = await fetch(url)

        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json()


            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const title = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const startDate = details.conference.starts
                    const endDate = details.conference.ends
                    const html = createCard(title, description, pictureUrl, startDate, endDate)
                    // const column = document.querySelector('.col')
                    container.innerHTML += html;
                    console.log(new Date(startDate))




                }
            }

        }
    } catch (e) {
        // Figure out what to do if an error is raised
    }

})
